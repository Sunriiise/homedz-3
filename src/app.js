﻿import './styles/style.scss';
import './fonts/fonts.scss';
import './media_queries/media.scss';
import './js/scripts.js';
import './js/owl_scripts.js';
import './js/menu.js';